__author__ = "Jaime Rivera <jaime.rvq@gmail.com>"
__copyright__ = "Copyright 2022, Jaime Rivera"
__credits__ = []
__license__ = "MIT License"


from all_nodes.logic.logic_node import GeneralLogicNode


class GetHoudiniVersion(GeneralLogicNode):

    OUTPUTS_DICT = {"hou_version": {"type": str}}

class GetHoudiniFramerange(GeneralLogicNode):

    OUTPUTS_DICT = {
        "start_frame": {"type": int},
        "end_frame": {"type": int},
    }
